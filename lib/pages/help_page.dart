import 'package:flutter/material.dart';

class HelpPage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: HelpPageState(),
      ),
    );
  }
}

/// This is the stateful widget that the main application instantiates.
class HelpPageState extends StatefulWidget {
  @override
  State<HelpPageState> createState() => _HelpPageState();
}

/// This is the private State class that goes with MyStatefulWidget.
class _HelpPageState extends State<HelpPageState> {

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child:  Column(
        children: [
          Stack(
            children: [
              Image.asset(
                'assets/mask.jpg',
              ),
              Container(
                alignment: Alignment.center,
                child: Padding(
                  padding: EdgeInsets.fromLTRB(0, 45, 235, 0),
                  child: Column(
                    children: [
                      Text(
                        'Pusat',
                        textAlign: TextAlign.left,
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 20,
                          color: Colors.grey[300],
                        ),
                      ),
                      Text(
                        'Bantuan',
                        textAlign: TextAlign.left,
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 20,
                          color: Colors.grey[300],
                        ),
                      ),
                    ],
                  )
                ),
              ),
            ],
          ),
          SizedBox(
            height: 418,
            width: 418,
            child: Container(
              color: Colors.grey[300],
              child: Column(
                children: [
                  Align(
                    alignment: Alignment.centerLeft,
                    child: Padding(
                        padding: EdgeInsets.symmetric(horizontal: 30, vertical: 10),
                        child: Text(
                          'Pusat Bantuan',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 20,
                          ),
                        ),
                      )
                    ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 30),
                    child: Text(
                      'Silahkan hubungi melalui informasi yang tersedia di bawah jika Anda mengalami gejala-gejala Covid-19.',
                      textAlign: TextAlign.justify,
                      style: TextStyle(
                        fontSize: 12,
                      ),
                    )
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal:25, vertical: 8),
                    child: SizedBox(
                      height:60,
                      child: Card(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            Flexible(
                              flex:1,
                              child:Icon(
                                Icons.phone,
                                color: Colors.green,
                              ),
                            ),
                            Flexible(
                              flex:1,
                              child: Align(
                                alignment: Alignment.centerLeft,
                                child: Text(
                                  'HOTLINE',
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              )
                            ),
                            Flexible(
                              flex:1,
                              child:  ElevatedButton(
                                style: ButtonStyle(
                                  foregroundColor: MaterialStateProperty.all<Color>(Colors.blue),
                                ),
                                onPressed: () {},
                                child: Text(
                                  'PANGGIL',
                                  style: TextStyle(
                                    color:Colors.white,
                                  ),
                                ),
                              ),
                            )
                          ],
                        )
                      ),
                    )
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal:25, vertical: 8),
                    child: SizedBox(
                      height: 60,
                      child: Card(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            Flexible(
                              flex:1,
                              child:Icon(
                                Icons.people,
                                color: Colors.orange,
                              ),
                            ),
                            Flexible(
                              flex:2,
                              child: Align(
                                alignment: Alignment.centerLeft,
                                child: Text(
                                  'Konsultasi Dokter',
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              )
                            ),
                            Flexible(
                              flex:1,
                              child:Icon(
                                Icons.arrow_right,
                                color: Colors.grey,
                              ),
                            )
                          ],
                        )
                      ),
                    )
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal:25, vertical: 8),
                    child: SizedBox(
                      height: 60,
                      child: Card(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            Flexible(
                              flex:1,
                              child:Icon(
                                Icons.local_hospital,
                                color: Colors.purple,
                              ),
                            ),
                            Flexible(
                              flex:2,
                              child: Align(
                                alignment: Alignment.centerLeft,
                                child: Text(
                                  'Rumah Sakit Terdekat',
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              )
                            ),
                            Flexible(
                              flex:1,
                              child:Icon(
                                Icons.arrow_right,
                                color: Colors.grey,
                              ),
                            )
                          ],
                        )
                      ),
                    )
                  ),
                ],
              )
            ),
          ),
        ],
      )
    );
  }
}
