import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

class HomePage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: HomePageState(),
      ),
    );
  }
}

/// This is the stateful widget that the main application instantiates.
class HomePageState extends StatefulWidget {
  @override
  State<HomePageState> createState() => _HomePageState();
}

/// This is the private State class that goes with MyStatefulWidget.
class _HomePageState extends State<HomePageState> {
  String dropdownValue = 'Kalimantan Barat';

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Column(
          children: [
            Stack(
              children: [
                Image.asset(
                  'assets/mask.jpg',
                ),
                Container(
                  alignment: Alignment.center,
                  child: Padding(
                    padding: EdgeInsets.fromLTRB(0, 30, 235, 0),
                    child: Column(
                      children: [
                        Text(
                          'Lawan',
                          textAlign: TextAlign.left,
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 20,
                            color: Colors.grey[300],
                          ),
                        ),
                        Text(
                          'Covid-19',
                          textAlign: TextAlign.left,
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 20,
                            color: Colors.grey[300],
                          ),
                        ),
                        Text(
                          '#DirumahAja',
                          textAlign: TextAlign.left,
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 20,
                            color: Colors.grey[300],
                          ),
                        ),
                      ],
                    )
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 418,
              width: 418,
              child: Container(
                alignment: Alignment.center,
                color: Colors.grey[300],
                child: Column(
                  children: [
                    Padding(
                      padding: EdgeInsets.all(15),
                      child: Card(
                        child: Container(
                          width: 300,
                          margin: EdgeInsets.symmetric(horizontal: 10),
                          child: DropdownButton<String>(
                            value: dropdownValue,
                            icon: const Icon(Icons.arrow_right),
                            iconSize: 24,
                            elevation: 16,
                            style: const TextStyle(color: Colors.blue),
                            underline: Container(
                              height: 2,
                              color: Colors.blue,
                            ),
                            onChanged: (String? newValue) {
                              setState(() {
                                dropdownValue = newValue!;
                              });
                            },
                            items: <String>['Kalimantan Barat', 'Kalimantan Timur', 'Kalimantan Tengah'].map<DropdownMenuItem<String>>((String value) {
                              return DropdownMenuItem<String>(
                                value: value,
                                child: Text(value),
                              );
                            }).toList(),
                          ),
                        )
                      ),
                    ),

                    Card(
                      child: Container(
                        width: 320,
                        color: Colors.blue[300],
                        child: Padding(
                          padding: EdgeInsets.all(10),
                            child: Column(
                            children: [
                              Text(
                                'Kasus Covid-19 Terkonfirmasi', 
                                style: TextStyle(
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white,
                                ),
                              ),
                              Text(
                                '10',
                                style: TextStyle(
                                  fontSize: 60,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white,
                                ),
                              ),
                            ],
                          )
                        )
                      ),
                    ),
                    SizedBox(
                      height:0,
                    ),
                    Card(
                      child: Container(
                        width: 320,
                        color: Colors.white,
                        child: Padding(
                          padding: EdgeInsets.all(10),
                          child: Column(
                            children: [
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text('Sembuh'),
                                  Text('2', style: TextStyle(color: Colors.green),),
                                ],
                              ),
                              const Divider(
                                height: 15,
                                thickness: 1,
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text('Meninggal'),
                                  Text('2', style: TextStyle(color: Colors.red[500]),),
                                ],
                              ),
                              const Divider(
                                height: 15,
                                thickness: 1,
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text('Dalam Perawatan'),
                                  Text('6', style: TextStyle(color: Colors.red[300]),),
                                ],
                              ),
                              const Divider(
                                height: 15,
                                thickness: 1,
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text('Orang dalam Pemantauan'),
                                  Text('5015', style: TextStyle(color: Colors.blue),),
                                ],
                              ),
                              const Divider(
                                height: 15,
                                thickness: 1,
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text('Pasien dalam Pengawasan'),
                                  Text('31', style: TextStyle(color: Colors.yellow[600]),),
                                ],
                              ),
                              const Divider(
                                height: 15,
                                thickness: 1,
                              ),
                            ],
                          )
                        )
                      ),
                    ),
                  ],
                )
              ),
            ),
          ],
        )
    );
  }
}
