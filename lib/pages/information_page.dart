import 'package:flutter/material.dart';

class InformationPage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: InformationPageState(),
      ),
    );
  }
}

/// This is the stateful widget that the main application instantiates.
class InformationPageState extends StatefulWidget {
  @override
  State<InformationPageState> createState() => _InformationPageState();
}

/// This is the private State class that goes with MyStatefulWidget.
class _InformationPageState extends State<InformationPageState> {

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child:  Column(
        children: [
          Stack(
            children: [
              Image.asset(
                'assets/mask.jpg',
              ),
              Container(
                alignment: Alignment.center,
                child: Padding(
                  padding: EdgeInsets.fromLTRB(0, 45, 235, 0),
                  child: Column(
                    children: [
                      Text(
                        'Kenali,',
                        textAlign: TextAlign.left,
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 20,
                          color: Colors.grey[300],
                        ),
                      ),
                      Text(
                        'Covid-19',
                        textAlign: TextAlign.left,
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 20,
                          color: Colors.grey[300],
                        ),
                      ),
                    ],
                  )
                ),
              ),
            ],
          ),
          SizedBox(
            height: 418,
            width: 418,
            child: Container(
              color: Colors.grey[300],
              child: Column(
                children: [
                  Align(
                    alignment: Alignment.centerLeft,
                    child: Padding(
                        padding: EdgeInsets.symmetric(horizontal: 30, vertical: 10),
                        child: Text(
                          'Apa itu virus Corona ?',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 20,
                          ),
                        ),
                      )
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal:25, vertical: 8),
                    child: SizedBox(
                      height: 60,
                      child: Card(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            Flexible(
                              flex:1,
                              child:Icon(
                                Icons.search,
                                color: Colors.blue,
                              ),
                            ),
                            Flexible(
                              flex:2,
                              child: Align(
                                alignment: Alignment.centerLeft,
                                child: Text(
                                  'Mengenal',
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              )
                            ),
                            Flexible(
                              flex:1,
                              child:Icon(
                                Icons.arrow_right,
                                color: Colors.grey,
                              ),
                            )
                          ],
                        )
                      ),
                    )
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal:25, vertical: 8),
                    child: SizedBox(
                      height: 60,
                      child: Card(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            Flexible(
                              flex:1,
                              child:Icon(
                                Icons.admin_panel_settings,
                                color: Colors.orange,
                              ),
                            ),
                            Flexible(
                              flex:2,
                              child: Align(
                                alignment: Alignment.centerLeft,
                                child: Text(
                                  'Mencegah',
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              )
                            ),
                            Flexible(
                              flex:1,
                              child:Icon(
                                Icons.arrow_right,
                                color: Colors.grey,
                              ),
                            )
                          ],
                        )
                      ),
                    )
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal:25, vertical: 8),
                    child: SizedBox(
                      height: 60,
                      child: Card(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            Flexible(
                              flex:1,
                              child:Icon(
                                Icons.check,
                                color: Colors.green,
                              ),
                            ),
                            Flexible(
                              flex:2,
                              child: Align(
                                alignment: Alignment.centerLeft,
                                child: Text(
                                  'Mengobati',
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              )
                            ),
                            Flexible(
                              flex:1,
                              child:Icon(
                                Icons.arrow_right,
                                color: Colors.grey,
                              ),
                            )
                          ],
                        )
                      ),
                    )
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal:25, vertical: 8),
                    child: SizedBox(
                      height: 60,
                      child: Card(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            Flexible(
                              flex:1,
                              child:Icon(
                                Icons.dangerous,
                                color: Colors.yellow[600],
                              ),
                            ),
                            Flexible(
                              flex:2,
                              child: Align(
                                alignment: Alignment.centerLeft,
                                child: Text(
                                  'Mengantisipasi',
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              )
                            ),
                            Flexible(
                              flex:1,
                              child:Icon(
                                Icons.arrow_right,
                                color: Colors.grey,
                              ),
                            )
                          ],
                        )
                      ),
                    )
                  ),
                ],
              )
            ),
          ),
        ],
      )
    );
  }
}
